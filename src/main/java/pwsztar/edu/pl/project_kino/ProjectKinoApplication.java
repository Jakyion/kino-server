package pwsztar.edu.pl.project_kino;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
public class ProjectKinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectKinoApplication.class, args);
	}


}
