package pwsztar.edu.pl.project_kino.service;

import java.io.File;

public interface EmailSender {
    void sendEmail(String to, String subject, String content, File file);
}