package pwsztar.edu.pl.project_kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import pwsztar.edu.pl.project_kino.dto.Zamowienia;

import java.util.List;

@Service
public class ZamowieniaService {

    private final ZamowieniaRepository zamowieniaRepository;

    @Autowired
    public ZamowieniaService(ZamowieniaRepository zamowieniaRepository) {
        this.zamowieniaRepository = zamowieniaRepository;
    }

    @CrossOrigin
    @GetMapping()
    public List<Zamowienia> getZamowienia()
    {
        return zamowieniaRepository.findAll();
    }


    public void saveZamowienie(int id_seansu, String imie,String nazwisko, String email)
    {
        Zamowienia zamowienia = new Zamowienia(id_seansu,imie,nazwisko,email,"t");
        zamowieniaRepository.save(zamowienia);
    }

}
