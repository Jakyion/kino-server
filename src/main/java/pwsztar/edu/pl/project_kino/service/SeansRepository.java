package pwsztar.edu.pl.project_kino.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pwsztar.edu.pl.project_kino.dto.Seans;

@Repository
public interface SeansRepository extends JpaRepository<Seans,Integer> {

}
