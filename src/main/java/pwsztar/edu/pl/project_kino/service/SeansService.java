package pwsztar.edu.pl.project_kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsztar.edu.pl.project_kino.Response.RezerwacjaResponse;
import pwsztar.edu.pl.project_kino.dto.Film;
import pwsztar.edu.pl.project_kino.dto.Rezerwacja;
import pwsztar.edu.pl.project_kino.dto.Seans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class SeansService {

    private final SeansRepository seansRepository;
    @Autowired
    public SeansService(SeansRepository seansRepository) {
        this.seansRepository = seansRepository;
    }

    public Seans getSeans(int id)
    {
        return seansRepository.findById(id).get();
    }

    public List<Seans> getSeanses() {
        return seansRepository.findAll();
    }

    public void saveSeat(int id_seansu,int miejsce,String typ)
    {
        Seans seans;
        seans = seansRepository.findById(id_seansu).get();
        Boolean[] miejsca_zwykle = seans.getMiejsca_zwykle();
        Boolean[] miejsca_vip = seans.getMiejsca_vip();

        if(typ.equals("vip") && miejsca_vip[miejsce]!=true)
        {
            miejsca_vip[miejsce]=true;
        }
        else if(typ.equals("normalny") && miejsca_zwykle[miejsce]!=true)
        {
            miejsca_zwykle[miejsce]=true;
        }

        seansRepository.save(seans);

    }

    public RezerwacjaResponse checkIfAvaliavleV2(Rezerwacja rezerwacja,int id_seansu,int miejsce)
    {
        Seans seans;
        seans=seansRepository.findById(id_seansu).get();
        Boolean[] miejsca_zwykle = seans.getMiejsca_zwykle();
        Boolean[] miejsca_vip = seans.getMiejsca_vip();
        RezerwacjaResponse response = new RezerwacjaResponse();

        if(rezerwacja.getTyp().equals("vip"))
        {

            if(miejsca_vip[miejsce])response.setValidated(false);
            else response.setValidated(true);

        }
        else if(rezerwacja.getTyp().equals("normalny"))
        {

            if(miejsca_zwykle[miejsce])response.setValidated(false);
            else response.setValidated(true);
        }

        return response;

    }



    public void addSeanses(ArrayList<Seans> seanses) {

        try {

            List<Seans> seansesToGetId = getSeanses();
            ArrayList<Integer> indeksy = new ArrayList<>();

            for (Seans seansToGetId : seansesToGetId) {

                indeksy.add(seansToGetId.getId_se());
            }

            int max_index = Collections.max(indeksy) + 1;

//            for(Integer xd: indeksy){
//                System.out.println(xd);
//            }

            System.out.println(max_index + "   xDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

            for (Seans seans : seanses) {
                System.out.println(seans.getTytul() + "xddd");
                seansRepository.save(new Seans(
                        max_index,
                        seans.getId_sali(),
                        seans.getIdf(),
                        seans.getDzien(),
                        seans.getGodzina(),
                        seans.getMiejsca_zwykle(),
                        seans.getMiejsca_vip(),
                        seans.getTytul(),
                        seans.getZdjecie(),
                        seans.getCena_zwykly(),
                        seans.getCena_vip()));
                max_index += 1;
                TimeUnit.SECONDS.sleep(2);
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public void deleteSeanses(ArrayList<Integer> seanses) {

        System.out.println("usuwanie");
        for (Integer seans : seanses) {
            Seans toDelete =  seansRepository.getOne(seans);
            seansRepository.delete(toDelete);
        }

    }




}
