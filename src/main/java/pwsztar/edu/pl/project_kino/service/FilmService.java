package pwsztar.edu.pl.project_kino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsztar.edu.pl.project_kino.dto.Film;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class FilmService {

    private final FilmRepository filmRepository;

    @Autowired
    public FilmService(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    public Film getFilm(int id)
    {
        return filmRepository.findById(id).get();
    }
    
    public List<Film> getFilms()
    {
        return filmRepository.findAll();
    }


    public void addFilms(ArrayList<Film> films) {

        try {

            List<Film> filmsToGetId = getFilms();
            ArrayList<Integer> indeksy = new ArrayList<>();

            for (Film filmToGetId : filmsToGetId) {

                indeksy.add(filmToGetId.getIdf());
            }

            int max_index = Collections.max(indeksy) + 1;

            for (Film film : films) {
                System.out.println(film.getTytul() + "xddd");
                filmRepository.save(new Film(max_index, film.getTytul(), film.getGatunek(), film.getProdukcja(), film.getRok_premiery(),
                        film.getRezyser(), film.getCzas_trwania(), film.getWiek(), film.getOpis(), film.getZdjecie()));
                max_index += 1;
                TimeUnit.SECONDS.sleep(2);
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public void deleteFilms(ArrayList<Film> films) {

        System.out.println("usuwanie");
        for (Film film : films) {
            System.out.println(film);
            filmRepository.delete(film);
        }

    }




    }



