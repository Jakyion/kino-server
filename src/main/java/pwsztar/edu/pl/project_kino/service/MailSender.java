package pwsztar.edu.pl.project_kino.service;

import pwsztar.edu.pl.project_kino.dto.Newsletter;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

public class MailSender {

    private static final String sender="kino.noreply@gmail.com";
    private static final String password="AdminAdmin1234";
    private static String destination="";
    private static Session sesja;
    private static Properties props;

    /**
     * Metoda konfigurująca parametry dotyczące wysyłania maili
     */

    private static void setConfiguration()
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender, password);
            }
        });
        sesja=session;
        props=properties;

    }

    /**
     * Metoda sprawdza czy adres e-mail jest poprawny, zwraca true lub false w zazleżności od po
     * @param adres adres e-mail w postaci zmiennej typu String
     * @return Wartość typu Boolen
     */

    private boolean validation(String adres){

        boolean isValid = false;

        try {
            InternetAddress internetAddress = new InternetAddress(adres);
            internetAddress.validate();
            isValid=true;
        }
        catch (AddressException e)
        {
            e.printStackTrace();

        }

        return isValid;
    }

    /**
     * Kontruktor klasy MailSender
     */

    MailSender()
    {
        setConfiguration();
    }

    /**
     * Metoda wysyłająca wiadomości dla tablicy adresów e-mail które zostały jej podane, przyjmuje też tablicę zawierającą pozowolenia
     * zgodę na wysyłanie e-maili na podane wcześniej adresy e-mail
     *
     */

    public void sendMessages(List<Newsletter> lista,String subject, String message_content)  {

            for (Newsletter mail : lista) {

                if (mail.getZgoda() == true) {
                    destination = mail.getEmail();
                }

                if (destination != "" && validation(destination) == true)
                    try {
                        Message message = new MimeMessage(sesja);
                        message.setFrom(new InternetAddress(sender));
                        message.setRecipients(
                                Message.RecipientType.TO,
                                InternetAddress.parse(destination)
                        );

                        message.setSubject(subject);
                        message.setText(message_content);

                        if (validation(sender) && validation(destination)) {
                            Transport.send(message);
                            System.out.println("Email sent");
                        } else System.out.println("At least on of the addresses is not valid");


                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }

                destination = "";


            }


    }

}