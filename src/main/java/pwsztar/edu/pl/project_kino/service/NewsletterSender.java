package pwsztar.edu.pl.project_kino.service;


import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import pwsztar.edu.pl.project_kino.dto.Film;
import pwsztar.edu.pl.project_kino.dto.Newsletter;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Configuration
@EnableScheduling
public class NewsletterSender {

    private static List<Film> database_films;

    private final FilmService filmService;
    private final NewsletterService newsletterService;

    public NewsletterSender(NewsletterService newsletterService, FilmService filmService) {
        this.filmService = filmService;
        this.newsletterService = newsletterService;
        database_films=null;
    }


    @Scheduled(cron = "0 0 0/1 * * ?")
    public void checkDataBase()
    {
        System.out.println("metoda");
        List<Film> films;
        List<Film> newFilms = new ArrayList<>();
        if(database_films == null)
        {
            database_films=filmService.getFilms();
        }
        else
        {
            films=filmService.getFilms();

//            System.out.println(database_films.size());
//            System.out.println(films.size());

            if(films.size()>database_films.size())
            {

                for(int i=database_films.size()+1;i<=films.size();i++)
                {
                        newFilms.add(filmService.getFilm(i));

                }

                newsletterService.sendEmails(newFilms);
            }
            database_films=filmService.getFilms();



//            System.out.println(newFilms);

        }

    }
}
