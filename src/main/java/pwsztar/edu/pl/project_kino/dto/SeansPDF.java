package pwsztar.edu.pl.project_kino.dto;

import lombok.Data;

@Data
public class  SeansPDF {
    private String nazwaSeansu;
    private String dataSeansu;
    private String imieNazwisko;
    private Integer rzad;
    private Integer miejsce;
    private String qr;


}